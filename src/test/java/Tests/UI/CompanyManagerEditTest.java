package Tests.UI;

import ui.models.CompanyManagerModel;
import ui.models.CompanyModel;
import ui.steps.*;
import ui.utils.CompanyUtil;
import ui.utils.CompanyManagerUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CompanyManagerEditTest {

    private MainPageSteps mainPageSteps = new MainPageSteps();
    private LoginSteps login = new LoginSteps();
    private CompanyAddAndSearchSteps companySearch = new CompanyAddAndSearchSteps();
    private CompanyAddAndSearchSteps createCompanyYL = new CompanyAddAndSearchSteps();
    private RequiredFieldsButtonCheckingSteps requiredFieldsButtonCheckingSteps = new RequiredFieldsButtonCheckingSteps();
    private CompanySteps companySteps = new CompanySteps();

    @BeforeClass
    public void setUp() {
        CompanyModel companyModel = new CompanyUtil().createCompany();
        mainPageSteps.openMainPage();
        login.authorization();
        requiredFieldsButtonCheckingSteps.checkingButtonRequiredFields();
        createCompanyYL.createCompanyYL(companyModel);
        companySearch.searchCompanyByNameAndOpen(companyModel);
    }

    @Test(groups = {"smoke"}, description = "Менеджер Наймикса/Компании/Карточка компании ЮЛ/Информация/Руководитель компании - редактирование",dependsOnGroups={"CompanyCreateTest.smoke"})
    public void companyManagerEdit() {
        CompanyManagerModel companyManagerModel = new CompanyManagerUtil().editCompanyManager();
        companySteps.editCompanyManager(companyManagerModel);
        companySteps.checkEditCompanyManager(companyManagerModel);
    }

    @AfterClass
    public void closeUp() {
        mainPageSteps.closeMainPage();
    }
}
