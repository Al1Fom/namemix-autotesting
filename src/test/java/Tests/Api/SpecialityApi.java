package Tests.Api;

import api.utils.ApiBuilder;
import db.utils.DatabaseBuilder;
import db.interfaces.SpecialityDao;
import ui.models.AuthApiModel;
import db.models.SpecialityDbModel;
import api.models.SpecialityModel;
import api.models.SpecialityResultModel;
import org.jdbi.v3.core.Jdbi;
import org.testng.Assert;
import org.testng.annotations.Test;
import retrofit2.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class SpecialityApi {
    @Test
    public void getSpecialityList() throws IOException, SQLException {
        ApiBuilder retrofit = new ApiBuilder();
        Jdbi jdbi =  DatabaseBuilder.getConnection();

        retrofit2.Response<AuthApiModel> auth = AuthorizationApi.getAuth();
        Response<SpecialityResultModel> speciality = retrofit.getApi().getSpecialityList("Bearer " + auth.body().getAccessToken()).execute();
        Assert.assertEquals(speciality.code(), 200);

        SpecialityModel specialityModelApi = speciality.body().getResults().get(0);

        List<SpecialityDbModel> specialityModelList = jdbi.withExtension(SpecialityDao.class, dao -> dao.listSpeciality(specialityModelApi.getId()));
        Assert.assertEquals(specialityModelList.get(0).getValue(), specialityModelApi.getValue());
        Assert.assertEquals(specialityModelList.get(0).getId(), specialityModelApi.getId());

    }
}
