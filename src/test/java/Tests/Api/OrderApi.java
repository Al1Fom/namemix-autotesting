package Tests.Api;

import api.utils.ApiBuilder;
import db.utils.DatabaseBuilder;
import ui.models.AuthApiModel;
import ui.models.CreateOrderApiModel;
import ui.models.CreateOrderBodyApiModel;
import org.jdbi.v3.core.Jdbi;
import org.testng.Assert;
import org.testng.annotations.Test;
import retrofit2.Response;

import java.io.IOException;
import java.sql.SQLException;

public class OrderApi {

    @Test
    public void createOrder() throws IOException, SQLException {
        CreateOrderBodyApiModel crateOrderBody = new CreateOrderBodyApiModel();
        ApiBuilder retrofit = new ApiBuilder();
        Jdbi jdbi =  DatabaseBuilder.getConnection();

        Response<AuthApiModel> auth = AuthorizationApi.getAuth();
        Response<CreateOrderApiModel> order = retrofit.getApi().createOrderApi(crateOrderBody, "Bearer " + auth.body().getAccessToken()).execute();
        Assert.assertEquals(order.code(), 200);
        Assert.assertNull(order.body().getErrorCode());
        Assert.assertEquals(order.body().getRequestId(), crateOrderBody.requestId);
        Assert.assertEquals(order.body().getContractorInn(), crateOrderBody.contractorInn);
        Assert.assertEquals(order.body().getPaymentAmount(), crateOrderBody.orderAmount);

       // List<String> order = jdbi.withHandle(handle -> handle.createQuery("select "))

    }

}
