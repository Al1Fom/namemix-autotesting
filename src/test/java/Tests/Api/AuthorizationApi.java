package Tests.Api;


import api.utils.ApiBuilder;
import ui.models.AuthApiModel;
import api.models.RegistrationBodyApiModel;
import org.testng.Assert;
import org.testng.annotations.Test;
import retrofit2.Response;

import java.io.IOException;

public class AuthorizationApi {

    public static Response<AuthApiModel> getAuth() throws IOException {
        RegistrationBodyApiModel body = new RegistrationBodyApiModel();
        ApiBuilder retrofit = new ApiBuilder();

        body.login = System.getenv("LOGIN_API");
        body.password = System.getenv("PASSWORD_API");

        Response<AuthApiModel> response = retrofit.getApi().authorizationApi(body).execute();//Отправка запрса

        return response;
    }

    @Test
    public void login() throws IOException {
        Response<AuthApiModel> response = getAuth();
        Assert.assertEquals(response.code(), 200);
        Assert.assertNotNull(response.body().getAccessToken());
        Assert.assertNull(response.body().getErrorCode());
    }

}
