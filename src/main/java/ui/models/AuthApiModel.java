package ui.models;


public class AuthApiModel {

    private String accessToken;
    private String refreshToken;
    private String role;
    private String clientId;
    private String clientUserId;
    private String errorMessage;
    private String errorCode;
    private String accessTokenExpirationDateTimeUTC;
    private String refreshTokenExpirationDateTimeUTC;
    private String loginActive;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientUserId() {
        return clientUserId;
    }

    public void setClientUserId(String elementPureHtml) {
        this.clientUserId = clientUserId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getAccessTokenExpirationDateTimeUTC() {
        return accessTokenExpirationDateTimeUTC;
    }

    public void setAccessTokenExpirationDateTimeUTC(String accessTokenExpirationDateTimeUTC) {
        this.accessTokenExpirationDateTimeUTC = accessTokenExpirationDateTimeUTC;
    }

    public String getRefreshTokenExpirationDateTimeUTC() {
        return refreshTokenExpirationDateTimeUTC;
    }

    public void setRefreshTokenExpirationDateTimeUTC(String refreshTokenExpirationDateTimeUTC) {
        this.refreshTokenExpirationDateTimeUTC = refreshTokenExpirationDateTimeUTC;
    }

    public String getLoginActive() {
        return loginActive;
    }

    public void setLoginActive(String loginActive) {
        this.loginActive = loginActive;
    }
}
