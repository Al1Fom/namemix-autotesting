package ui.models;

public class CreateOrderBodyApiModel {
    public String requestId;
    public String contractorPhone;
    public String contractorInn;
    public String contractorFirstName;
    public String contractorLastName;
    public String contractorPatronymic;
    public boolean contractorPaymentAgreementRequired;
    public String orderName;
    public String orderDescription;
    public int orderAmount;
    public String orderWorkEndDate;
    public String orderWorkStartDate;
    public String createActsOfAcceptanceOfWorkType;

    public String getRequestId() {
        return requestId;
    }

    public CreateOrderBodyApiModel setRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public String getContractorPhone() {
        return contractorPhone;
    }

    public CreateOrderBodyApiModel setContractorPhone(String contractorPhone) {
        this.contractorPhone = contractorPhone;
        return this;
    }

    public String getContractorInn() {
        return contractorInn;
    }

    public CreateOrderBodyApiModel setContractorInn(String contractorInn) {
        this.contractorInn = contractorInn;
        return this;
    }

    public String getContractorFirstName() {
        return contractorFirstName;
    }

    public CreateOrderBodyApiModel setContractorFirstName(String contractorFirstName) {
        this.contractorFirstName = contractorFirstName;
        return this;
    }

    public String getContractorLastName() {
        return contractorLastName;
    }

    public CreateOrderBodyApiModel setContractorLastName(String contractorLastName) {
        this.contractorLastName = contractorLastName;
        return this;
    }

    public String getContractorPatronymic() {
        return contractorPatronymic;
    }

    public CreateOrderBodyApiModel setContractorPatronymic(String contractorPatronymic) {
        this.contractorPatronymic = contractorPatronymic;
        return this;
    }

    public boolean isContractorPaymentAgreementRequired() {
        return contractorPaymentAgreementRequired;
    }

    public CreateOrderBodyApiModel setContractorPaymentAgreementRequired(boolean contractorPaymentAgreementRequired) {
        this.contractorPaymentAgreementRequired = contractorPaymentAgreementRequired;
        return this;
    }

    public String getOrderName() {
        return orderName;
    }

    public CreateOrderBodyApiModel setOrderName(String orderName) {
        this.orderName = orderName;
        return this;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public CreateOrderBodyApiModel setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
        return this;
    }

    public int getOrderAmount() {
        return orderAmount;
    }

    public CreateOrderBodyApiModel setOrderAmount(int orderAmount) {
        this.orderAmount = orderAmount;
        return this;
    }

    public String getOrderWorkEndDate() {
        return orderWorkEndDate;
    }

    public CreateOrderBodyApiModel setOrderWorkEndDate(String orderWorkEndDate) {
        this.orderWorkEndDate = orderWorkEndDate;
        return this;
    }

    public String getOrderWorkStartDate() {
        return orderWorkStartDate;
    }

    public CreateOrderBodyApiModel setOrderWorkStartDate(String orderWorkStartDate) {
        this.orderWorkStartDate = orderWorkStartDate;
        return this;
    }

    public String getCreateActsOfAcceptanceOfWorkType() {
        return createActsOfAcceptanceOfWorkType;
    }

    public CreateOrderBodyApiModel setCreateActsOfAcceptanceOfWorkType(String createActsOfAcceptanceOfWorkType) {
        this.createActsOfAcceptanceOfWorkType = createActsOfAcceptanceOfWorkType;
        return this;
    }

}
