package ui.utils;

import ui.models.CompanyBankRepresentativeModel;

public class CompanyBankRepresentativeUtil {
    public CompanyBankRepresentativeModel editCompanyBankRepresentative(){
        return new CompanyBankRepresentativeModel()
                .setName("Владимир Владимирович Владимиров")
                .setBasedOn("на основании");
    }
}
