package ui.utils;

import ui.models.CreateOrderBodyApiModel;

import java.util.UUID;

public class CreateOrderApiModelUtil {
    public CreateOrderBodyApiModel createOrderModel(){
        return new CreateOrderBodyApiModel()
                .setRequestId(UUID.randomUUID().toString())
                .setContractorPhone("70000220074")
                .setContractorInn("440126580518")
                .setContractorFirstName("Сергей")
                .setContractorLastName("Носов")
                .setContractorPatronymic("Васильевич")
                .setContractorPaymentAgreementRequired(true)
                .setOrderName("надо что то сделат")
                .setOrderDescription("что и зачем сделать")
                .setOrderAmount(100)
                .setOrderWorkEndDate("2020-12-16")
                .setOrderWorkStartDate("2020-12-16")
                .setCreateActsOfAcceptanceOfWorkType("AUTOMATICALLY");
    }
}
