package ui.page;

import ui.forms.CompanyCardForm;
import ui.forms.CompanyProjectAndObjectForm;

public class CompanyCardPage {
    public CompanyCardForm onCompanyCardForm() {
        return new CompanyCardForm();
    }

    public CompanyProjectAndObjectForm onCompanyProjectAndObjectForm() {
        return new CompanyProjectAndObjectForm();
    }
}
