package db.utils;


import db.models.SpecialityDbModel;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class SpecialityMapper implements RowMapper<SpecialityDbModel> {

    @Override
    public SpecialityDbModel map(ResultSet rs, StatementContext ctx) throws SQLException {
        SpecialityDbModel speciality = new SpecialityDbModel();
        speciality.setId(UUID.fromString(rs.getString("id")));
        speciality.setValue(rs.getString("value"));
        return speciality;
    }
}
