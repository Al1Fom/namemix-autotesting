package db.utils;

import java.sql.SQLException;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.postgres.PostgresPlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

public class DatabaseBuilder {
    public static Jdbi getConnection() throws SQLException {
        Jdbi jdbi = Jdbi.create(System.getenv("URL_DB"), System.getenv("USER_DB"), System.getenv("PASSWORD_DB"));
        jdbi.installPlugin(new SqlObjectPlugin());
        jdbi.installPlugin(new PostgresPlugin());
        return jdbi;
    }

}
