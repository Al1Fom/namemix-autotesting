package db.interfaces;

import db.models.SpecialityDbModel;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

public interface SpecialityDao {

    @SqlQuery("SELECT \"id\", \"value\" FROM nmtest.\"SpecialitiesV2\" WHERE \"id\" = :id ")
    @RegisterBeanMapper(SpecialityDbModel.class)
    List<SpecialityDbModel> listSpeciality(@Bind("id") UUID id);
}
