package api.utils;

import api.interfaces.InterfaceApi;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiBuilder {
    private InterfaceApi interfaceApi;

    public ApiBuilder() {
        retrofit2.Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(System.getenv("URL_API")) //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        interfaceApi = retrofit.create(InterfaceApi.class); //Создаем объект, при помощи которого будем выполнять запросы

    }

    public InterfaceApi getApi() {
        return interfaceApi;
    }

}
