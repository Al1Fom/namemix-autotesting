package api.interfaces;

import api.models.RegistrationBodyApiModel;
import api.models.SpecialityResultModel;
import ui.models.AuthApiModel;
import ui.models.CreateOrderApiModel;
import ui.models.CreateOrderBodyApiModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface InterfaceApi {
    @POST("/nmapi/auth/login")
    Call<AuthApiModel> authorizationApi(@Body RegistrationBodyApiModel registrationBody);

    @POST("/nmapi/orders/addWithPayment")
    Call<CreateOrderApiModel> createOrderApi(@Body CreateOrderBodyApiModel registrationBody, @Header("Authorization") String authHeader);

    @GET("/nmapi/dictionary/v2/speciality/all")
    Call<SpecialityResultModel> getSpecialityList(@Header("Authorization") String authHeader);

}


