package api.models;

import java.util.List;

public class SpecialityResultModel {
    public List<SpecialityModel> getResults() {
        return results;
    }

    public void setResults(List<SpecialityModel> results) {
        this.results = results;
    }

    private List<SpecialityModel> results;
}
